package SDM::Conf;

=head1 NAME

SDM::Conf - Populate configuration for SDM perl code via external file

=head1 SYNOPSIS

 use SDM::Conf;

 my $cfg = SDM::Conf->new();

Uses the following environment variables

 SDM_DEBUG (optional, set to get verbose messages)
 SDM_CONFIG (if not set defaults to /noaosw/etc/sdm.conf)
 SDM_ENVMODE (if not set, must have host entry in config file)

=cut

# ------------------ DEPENDENCIES ---------------------

# minimum feature set (c'mon folks, it is 2013) - includes feature 'say'
# but it should work down to 5.10 if you are desperate
use v5.16.0;

# this module allows a nice format for human readable config files
use Config::IniFiles;
# use Moose OO framework; perldoc Moose::Manual for more.
# note that this automatically enables 'strict' and 'warnings'
use Moose;
# we'll need this for croak
use Carp;
# want this for hostname
use Sys::Hostname;
# use this for default location of sdm.cfg
use File::ShareDir qw/dist_file/;
# use smartmatch ( ~~ operator) with caution though
no warnings 'experimental::smartmatch';
our $VERSION = '0.03';
# we're going to need a global hash to hold the values
our %data;

# ----------------- TECHNICAL DEBT ---------------------

# Must have:

# - need to sort out a unix group for sdm with Josh so that
# the config file does not need to be world readable
# - more test coverage
# - turn some defaults to builders for more error checking

# Nice to haves:
# - create an SDM::Log class to handle the print statements
# - create an SDM::Error class to handle errors consistently
# - extra set of env definitions redundant. get rid of

# ------------------------ BUILD ------------------------

# the Moose BUILD function is called after the object construction

# here we are going to make sure the config file is ok and read it
# in

sub BUILD {
  my $self = shift;
  my $debug = $self->debug; # global debug level - we'll need this a lot
  my $filename = $self->config_file; # config filename - this one too


  say __PACKAGE__,"\t","Debug is on" if $self->debug;
  say __PACKAGE__,"\t","unsetenv SDM_DEBUG to turn of" if $debug;
  say __PACKAGE__,"\t","Config file set to ",$filename if $debug;


  # this will (correctly) terminate if the file does not exist
  my $inifile = Config::IniFiles->new( -file => $filename)
  or croak "Aii! No config file found - did you forget to set SDM_CONFIG?";

  # let's do some validation of the contents

  # first we need a list of environments (production, testing, etc)
  $inifile->SectionExists('environments')
  or croak "Aii! Required [environments] section not found in ",$filename;

  my @environments = $inifile->val('environments','modes');
  say __PACKAGE__,"\tFound modes: ",join(" ",@environments) if $debug;

  # if the environment variable SDM_ENVMODE is found, let us see if we
  # can use that

  my $mode;
  my $host = hostname;

  if ($ENV{'SDM_ENVMODE'}) {
    $mode = $ENV{'SDM_ENVMODE'};
    say __PACKAGE__,"\tUsing $mode mode set by SDM_ENVMODE" if $debug;
    # check to see if we have a match with supported values
    if ($mode ~~ @environments) {
      say __PACKAGE__,"\tFound mode $mode in $filename" if $debug;
      $self->envmode($mode);
    } else {
      croak "Aii! Mode $mode set by SDM_ENVMODE not found in $filename";
    }
  } else {
    # since we didn't find SDM_ENVMODE there had better be
    # a section for this host in the config file

    say __PACKAGE__,"\t","We appear to be running on $host" if $debug;
    $inifile->SectionExists($host)
    or croak "Aii! No section for $host found in ",$filename;
    # make sure the host has the mandatory mode parameter
    $inifile->val($host,"mode")
    or croak "Aii! No mode specified in $host section of ",$filename;

    # ensure the host's desired mode has a section with definitions
    $mode = $inifile->val($host,"mode");
    say __PACKAGE__,"\t","Configuring $host to $mode mode" if $debug;
    $inifile->SectionExists($mode)
    or croak "Aii! Host $host wants to be in $mode but no such section exists";
    $self->envmode($mode);
  }

  # finally, let's do something
#  our %data;
  # first we are going to read in the key value pairs from the
  # environment section (production etc)
  my @parameters = $inifile->Parameters($mode);
  say __PACKAGE__,"\t","Found $mode parameters: ",join(" ",@parameters) if $debug;

  if (@parameters) {
    foreach my $key (@parameters) {
      $data{$key} = $inifile->val($mode,$key);
      say __PACKAGE__,"\tKey: ",$key,"\tValue: ",$data{$key} if $debug;
    };
  };

  # now we read through the host key value pairs (if any), allowing it
  # to overwrite those specified in the environment section
  # (we're guaranteed mode, so not point checking for empty)

  @parameters = ();
  @parameters = $inifile->Parameters($host);
  say __PACKAGE__,"\t","Found $host parameters: ",join(" ",@parameters) if $debug;

  foreach my $key (@parameters) {
    $data{$key} = $inifile->val($host,$key);
    say __PACKAGE__,"\tKey: ",$key,"\tValue: ",$data{$key} if $debug;
  };

};

=head1 ATTRIBUTES

=cut

# -------------------- THE CONFIGURATION FILE  -------------

# here we are going to provide an attribute with the file location
# of the config file

=head2 config_file

 my $cfg = SDM::Conf->new();

 print $cfg->config_file;

This read-only attribute is the location of the wiring file for the
SDM configuration file.

It looks for the environment variable SDM_CONFIG.
If SDM_CONFIG is not set, it defaults to the conf/sdm.cfg of install directory

=cut

has 'config_file' => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    builder => '_config_file_default',
 );

# private builder method to set value (preference given to the
# environment variable first), then the copy in the install directory

sub _config_file_default  {
    my $self = shift;
    if ($ENV{SDM_CONFIG}) {
	say "Setting config file from SDM_CONFIG:",$ENV{SDM_CONFIG} if $self->debug;
	return  $ENV{SDM_CONFIG};
    } else {
      # dist_file is the method exported by File::ShareDir which
      # looks up the value set by share_dir in Build.PL
      my $conffile = dist_file('SDM','sdm.cfg');
      say "Setting config file to default value: $conffile" if $self->debug;
      return $conffile;
    };
};

# ----------------------- DEBUGGING -------------------------------

# here we provide an attribute to globally control debugging level


=head2 debug

Show debug statements if debug is on:

 my $cfg = SDM::Conf->new();
 print "Debug is on" if $cfg->debug;

or set the debug level:

 my $cfg = SDM::Conf->new();
 $cfg->debug(1);

This attribute returns or sets the debug level. If not explicitly set,
it first looks to the environment variable SDM_DEBUG, else it defaults
to 0.

=cut

has 'debug' => (
    is => 'rw',
    isa => 'Int',
    lazy => 1,
    builder => '_debug_default',
 );

# private builder to set default value

sub _debug_default   {
  if ($ENV{SDM_DEBUG}) {
      return $ENV{SDM_DEBUG}
    }
    else {
      return 0;
    }
};


# ----------------------- DEBUGGING -------------------------------

# The environment (production, sandbox etc) as passed down by the
# builder method

has 'envmode' => (
 is => 'rw',
 isa => 'Str',
);


# ------------------- DB ATTRIBUTES ----------------------

has 'dbserver' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 builder => '_dbserver_default',
);

sub _dbserver_default {
  # add some error checking ...
  my $dbserver = $data{'dbserver'};
  return $dbserver;
};

has 'dbport' => (
 is => 'ro',
 isa => 'Int',
 lazy => 1,
 default => sub {$data{'dbport'}},
);

has 'dbuser' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'dbuser'}},
);

has 'dbpwd' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'dbpwd'}},
);


# ------------------- LDAP ATTRIBUTES ----------------------

has 'ldapserver' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'ldapserver'}},
);

has 'ldapbase' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'ldapbase'}},
);

has 'ldapcn' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'ldapcn'}},
);

has 'ldappwd' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'ldappwd'}},
);

has 'ldapbackdoor' => (
  is => 'ro',
  isa => 'Str',
  lazy =>1,
  default => sub{$data{'ldapbackdoor'}},
);


# ------------------- MAIL ATTRIBUTES ----------------------

has 'mailhost' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'mailhost'}},
);

has 'mailuser' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'mailuser'}},
);

has 'mailpwd' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'mailpwd'}},
);


# ---------------------- PORTALS -------------------------------


has 'dataportal' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'dataportal'}},
);

has 'userportal' => (
 is => 'ro',
 isa => 'Str',
 lazy => 1,
 default => sub {$data{'userportal'}},
);

# ---------------------- MOP UP ---------------------------------

# speed up object construction
no Moose;
__PACKAGE__->meta->make_immutable;

1;

# remaining POD sections

# add your name here if you have worked on this code!

=head1 AUTHOR

Frossie Economou E<lt>frossie@noao.edu<gt>

=cut
