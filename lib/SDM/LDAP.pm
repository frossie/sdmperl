package SDM::LDAP;

=head1 NAME

SDM::LDAP - Perl class wrapping LDAP server interactions

=head1 SYNOPSIS



=head1 DESCRIPTION



=cut

# ------------------ DEPENDENCIES ---------------------

# use "modern" feature set - includes feature 'say' but it should work
# down to 5.10 if you are desperate
use v5.16.0;

# use Moose OO framework; perldoc Moose::Manual for more.
# note that this automatically enables 'strict' and 'warnings'
use Moose;
# we'll need this for croak
use Carp;
# we'll need this for accessing congiguration information
use SDM::Conf;
# we'll need this for accessing the LDAP server
use Net::LDAP;
our $VERSION = '0.03';

# ------------------ TECHNICAL DEBT ---------------------
#
# need private method to do the LDAP search and return the DN
# need better error checking from Net::LDAP returns
# need to eliminate direct use of superuser account. 
#
# ------------------ GLOBALS & BUILDER  ---------------------

# package-global variables
our $cfg;
our $debug;
our $ldap;

sub BUILD {

  my $self = shift;

  $cfg = SDM::Conf->new();
  $debug = 1 if $cfg->debug;
  say __PACKAGE__,"\t","Debug is on" if $debug;

   # let's find our LDAP server
   say "Running in SDM ",$cfg->envmode," mode" if $debug;
   $ldap = Net::LDAP->new($cfg->ldapserver ) or carp "$@";
   say 'Using LDAP server ',$cfg->ldapserver if $debug;

   # construct cn string for loging in
   my $lcn = 'cn='.$cfg->ldapcn;

   # let us authenticate as the master user
   my $mesg = $ldap->bind(
     $lcn,
     password => $cfg->ldappwd,
     ) or croak "$@";

 };


 # ---------------------- ATTRIBUTES ---------------------------------

 has 'server' => (
  is => 'ro',
  isa => 'Str',
  lazy => 1,
  default => sub {$cfg->ldapserver},
 );


 # ---------------------- METHODS ---------------------------------

=head2 passwords($uid, @passwords);

replaces current password entries for the specified user ID.

=cut

 sub passwords {

   my $self = shift;
   # the ID of the lucky contestant
   my $user = shift;
   # any other arguments assumed to be passwords for setting
   my @pwords = @_;

   # let's construct the ucn string

   my $ucn = '(cn='.$user.')';
   say __PACKAGE__,"\t","Looking up user ",$user if $debug;

   # perform the search
   my $result = $ldap->search( # perform a search
	 base   => $cfg->ldapbase,
	 filter => $ucn,
    ) or croak "$@";

   # let there be structure
   my $href = $result->as_struct;

   # in the DESCRIPTION pod expample @arrayOfDNs is the
   # 'cn=economouf,dc=sdm,dc=noao,dc=edu' level

   my @arrayOfDNs  = keys %$href;

   # There Can Only Be One


   if  ($#arrayOfDNs == -1) {
	 say  __PACKAGE__,"\t","No entries found for $user" if $debug;
	 return;
   } elsif ($#arrayOfDNs > 0) {
	 say  __PACKAGE__,"\t","Aiiii! Multiple entries for $user" if $debug;
	 return;
   };

   #

   my $key = shift(@arrayOfDNs);

   # if we were passed in passwords, let's change them
   if (@pwords) {

	 my $mesg = $ldap->modify( $key,
					    changes => [
					      replace => [
						userpassword => \@pwords,
						   ],
							 ]
						   );

   };

   my @pwds = @{ $href->{$key}->{'userpassword'} };

   if ($debug) {

	 for my $i (0..$#pwds) {
	   say  __PACKAGE__,"\t","Password $i ",$pwds[$i];
	 };

   };

 return(@pwds);

 };

=head2 respect_my_authority($uid,$password)

Returns 1 if authorised, 0 if not

=cut

 sub respect_my_authority {

   my $self = shift;
   my $uid = shift;
   my $password = shift;

   my  $myldap = Net::LDAP->new ($cfg->ldapserver ) or croak "$@";

   my $base = $cfg->ldapbase;

   my $ldapcn = "cn=".$uid.','.$base;
   my  $mesg = $ldap->bind ( $ldapcn,
			    password => $password,
			    version => 3 );

  $ldap->unbind;

  if ($mesg->code == 0) {
    # success
    return 1;
  } else {
    say  __PACKAGE__,"\t","LDAP code was ",$mesg->code if $debug;
    return 0;
  }

};

# ---------------------- MOP UP ---------------------------------


sub DEMOLISH {

my $mesg = $ldap->unbind;   # take down session

};

# speed up object construction
no Moose;
__PACKAGE__->meta->make_immutable;

1;

# remaining POD sections

=pod

=head DESCRIPTION

  print Dumper($href);

  $VAR1 = {
            'cn=economouf,dc=sdm,dc=noao,dc=edu' => {

              'Userpassword' => [
                             '{SSHA}nq6+funN ... Ag/j0trN==',
                             '{SSHA}kNbFujZF ... DdkOI3DDSeY/XcAw=='
                           ],
              'cn' => [
                    'frossie economou',
                    'economouf'
                 ],
              'homedirectory' => [
                              '/d1/ftp/protected/staging/economouf'
                            ],
              'sn' => [
                   'economou'
                 ],
               'objectclass' => [
                            'person',
                            'noaoarchiveuser',
                            'top'
                          ]
                }
          };

# add your name here if you have worked on this code!

=head1 AUTHOR

Frossie Economou E<lt>frossie@noao.edu<gt>

=cut
