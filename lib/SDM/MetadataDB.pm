package SDM::MetadataDB;

=head1 NAME

SDM::MetadataDB - Class for interacting with the SDM/NOAO Archive Metadata DB

=head1 SYNOPSIS

 use SDM::MetadataDB;
 my $db = SDM::MetadataDB->new();

 my $recipient = $db->have_noaoid_want_email($uid);

 my $noaoid = $db->have_email_want_noaoid('frossie@noao.edu');


=cut

# ----------------- TECHNICAL DEBT ---------------------

# Need to trap the posisbility of multiple returns from the SQL calls
# (shouldn't happen but Murphy's law and all that)

# ------------------ DEPENDENCIES ---------------------

# use "modern" feature set - includes feature 'say' but it should work
# down to 5.10 if you are desperate
use v5.16.0;
# use Moose OO framework; perldoc Moose::Manual for more.
# note that this automatically enables 'strict' and 'warnings'
use Moose;
# we'll need this for croak
use Carp;
# we'll need this for accessing congiguration information
use SDM::Conf;
#
use DBIx::Simple;

# package-global variables
our $cfg;
our $debug;
our $metadata;


sub BUILD {

  $cfg = SDM::Conf->new();
  # obey debug on/off from SDM::Conf
  $debug = $cfg->debug;
  say __PACKAGE__,"\t","Debug is on" if $debug;


  # let's construct the DBI spec
  my $dbi = 'DBI:Pg:dbname=metadata;host='.$cfg->dbserver.';port='.$cfg->dbport;
  # let's connect to the database

  $metadata = DBIx::Simple->connect (
    $dbi,
    $cfg->dbuser,
    $cfg->dbpwd,
    {RaiseError => 1}
   );

};

sub have_noaoid_want_email {
  my $self = shift;
  my $uid = shift;

  my $sql = "SELECT email_address FROM edu_noao_nsa.person WHERE noao_id = \'$uid\'";
  say __PACKAGE__,"\t",$sql if $debug;
  my ($email) = $metadata->query($sql)->list;
  say __PACKAGE__,"\t",$email if $debug;
  return $email;

};

sub have_email_want_noaoid {

my $self = shift;
my $email = shift;

my $sql = "SELECT noao_id FROM edu_noao_nsa.person WHERE email_address = \'$email\'";

say __PACKAGE__,"\t",$sql if $debug;
my ($noaoid) = $metadata->query($sql)->list;
say __PACKAGE__,"\t",$noaoid if $debug;
return $noaoid;
};

sub DEMOLISH {

  say __PACKAGE__,"\t","Demolishing..." if $debug;
  $metadata->disconnect;

};

# ---------------------- MOP UP ---------------------------------

# speed up object construction
no Moose;
__PACKAGE__->meta->make_immutable;

1;

# remaining POD sections

# add your name here if you have worked on this code!

=head1 AUTHOR

Frossie Economou E<lt>frossie@noao.edu<gt>

=cut
