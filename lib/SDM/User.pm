package SDM::User;

=head1 NAME

SDM::User - Class for descriptions of SDM systems users (PIs and coIs)

=head1 SYNOPSIS

 use SDM::User;
 my $user = SDM::User->new();
 $user->uid('norrisp');

 $user->email_user_noaoid('frossie@noao.edu')

 my $random = $user->generate_password;
 my @foo = $user->replace_password('economouf',$random);
 my $ok = $user->email_user_password('economouf',$random);

=cut

# ------------------ DEPENDENCIES ---------------------

# use "modern" feature set - includes feature 'say' but it should work
# down to 5.10 if you are desperate
use v5.16.0;

# use Moose OO framework; perldoc Moose::Manual for more.
# note that this automatically enables 'strict' and 'warnings'
use Moose;
# we'll need this for croak
use Carp;
# we'll need this for accessing congiguration information
use SDM::Conf;
# we'll need this for accessing LDAP methods
use SDM::LDAP;
# we need this for the method(s) that mail out
use Email::Sender::Simple qw(sendmail);
use Email::Simple;
use Email::Simple::Creator;
use Template;
use String::Validator::Password;
# use this for the location of the templates
use File::ShareDir qw/dist_dir/;
# DB stuff
use SDM::MetadataDB;
our $VERSION = '0.03';
# package-global variables
our $cfg;
our $debug;
our $tt;

sub BUILD {

  $cfg = SDM::Conf->new();

  # obey debug on/off from SDM::Conf
  $debug = $cfg->debug;
  say __PACKAGE__,"\t","Debug is on" if $debug;


  # set up the templates
  my $confdir = dist_dir('SDM');
  my $templatedir = $confdir."/templates";
  $tt = Template->new({
    INCLUDE_PATH => $templatedir,
    INTERPOLATE  => 1,
  }) || croak "$Template::ERROR\n";

};

# ----------------- TECHNICAL DEBT ---------------------
# must have:
# - generic method for emailing out requests (cut and paste AWOOGA)
# - improve test coverage

# ------------------- ATTRIBUTES -----------------------

=head1 ATTRIBUTES

=head2 uid

The noao user ID (lastname.firstinitial)

You can set this attribute in lieu of passing the $uid as a
parameter in certain methods.

=cut

has 'uid' => (
 is =>'rw',
 isa =>'Str',
);


# ---------------------- METHODS ---------------------------------

=head1 METHODS

=head2 replace_password($uid,$newpassword)

Method that given a uid and a password, replaces the uid's current
entry with the new password AND the NSA back door password

(Bad status is 0, good status returns the SHA arrays)

=cut

sub replace_password {

  my $self = shift;
  my $uid = shift;
  my $newpwd = shift;

  my $ldap = SDM::LDAP->new();
  my $ldapbackdoor = $cfg->ldapbackdoor;

  my @pwds = $ldap->passwords($uid,$newpwd,$cfg->ldapbackdoor);

  return @pwds;

};

=head2 generate_password

Method that given a uid, generates a pseudo-random password

It is assumed this will be used as a temporary password so no attempt
has been made to make it user memorable. If this is a future
requirement, see Crypt::PassGen

Bad status is 0, good status returns the password

=cut

sub generate_password {

  my $newpwd = "";
  my @chrs = ( 'A'..'N', 'P'..'Z', 'a'..'z', 0..9 );

for my $i (1..16) {
  my $r = $chrs[rand @chrs];
    $newpwd = $newpwd.$r
  }

return $newpwd

};


=head2 email_user_password($uid,$userpwd)

Method that given a uid and a cleartext password, emails the password
to the registered email address for that user

 my $status = $user->email_user_password('userid','secret');

(Bad status is 0, good status is 1)

=cut


sub email_user_password {

  my $self = shift;
  my $uid = shift;
  my $userpwd = shift;

  say __PACKAGE__,"\t","UID to email_user_password is $uid" if $debug;
  # set the UID attribute
  $self->uid($uid);
  # retrieve the password;

  # get the official sender email address
  my $sender = $cfg->mailuser;

  # this is the name of the template file to be used for the email
  my $template = 'send_pwd_reminder_email.tt';
  # these are the tokens referenced in the template
  my $tokens = {
    password => $userpwd,
    sdmemail => $sender,
    userportal => $cfg->userportal,
  };

  # process the template to generate the email body
  my $emailbody;
  $tt->process($template,$tokens,\$emailbody) ||
  croak $tt->error();

  # who do we send it to?
  my $db = SDM::MetadataDB->new();
  my $recipient = $db->have_noaoid_want_email($uid);
  say __PACKAGE__,"\t","recipient is $recipient" if $debug;
  
# send the email!
  my $email = Email::Simple->create(
    header => [
      To      => $recipient,
      From    => $sender,
      Subject => "Your NOAO Science Archive request",
     ],
    body => $emailbody,
   );

  sendmail($email);

  return 1;
};

=head2 email_user_noaoid

Method that given an email, emails the noaoid to that email address

 my $status = $user->email_user_noaoid('frossie@noao.edu');

(Bad status is 0, good status is 1)

=cut


sub email_user_noaoid {

  my $self = shift;
  my $email = shift;
  say __PACKAGE__,"\t","email to email_user_password is $email" if $debug;


  my $db = SDM::MetadataDB->new();

  my $noaoid = $db->have_email_want_noaoid($email);

  if ($noaoid) {
    say __PACKAGE__,"\t","user: $noaoid password: $noaoid" if $debug;
  } else {
    say __PACKAGE__,"\t","No email for $noaoid - bailing" if $debug;
   return 0;
  };


  # get the official sender email address
  my $sender = $cfg->mailuser;

  # this is the name of the template file to be used for the email
  my $template = 'send_noaoid_reminder_email.tt';
  # these are the tokens referenced in the template
  my $tokens = {
    noaoid => $noaoid,
    sdmemail => $sender,
  };

  # process the template to generate the email body
  my $emailbody;
  $tt->process($template,$tokens,\$emailbody) ||
  croak $tt->error();


  say __PACKAGE__,"\t","recipient is $email" if $debug;

# send the email!
  my $message = Email::Simple->create(
    header => [
      To      => $email,
      From    => $sender,
      Subject => "Your NOAO Science Archive request",
     ],
    body => $emailbody,
   );

  sendmail($message);

  return 1;
};


=head2 validate_email($userid,$email);

Given a userid and an email, returns true if the email associated with that
userid in the archive is the same as the email provided.

This can be used as a sanity check to verify that a user resetting their
password is correct about knowing what their userid is.

=cut

sub validate_email {

  my $self = shift;
  my $userid = shift;
  my $email = shift;

  my $db = SDM::MetadataDB->new();

  my $email_on_record = $db->have_noaoid_want_email($userid);

  return 1 if (lc($email) eq lc($email_on_record));

};


=head2 validate_password($password)

method to check whether password is not too simple

=cut

sub validate_password {

  my $self = shift;
  my $password = shift;

  # a naive implementation of the "Stanford Rules"
  # http://itservices.stanford.edu/service/accounts/passwords/quickguide

  my $lng = length($password);

  my $Validator;


  if ($lng < 8) {
     $Validator = String::Validator::Password->new(
       min_types => 1,
       min_len => 8,
      )
  } elsif ($lng >=8 && $lng < 12 ) {
       $Validator = String::Validator::Password->new(
       min_types => 4,
      )
    } elsif ($lng >= 12 && $lng < 16) {
       $Validator = String::Validator::Password->new(
	min_types => 3,
       )
    } elsif ($lng >=16 && $lng < 20) {
       $Validator = String::Validator::Password->new(
	min_types => 2,
       )
    } else {
       $Validator = String::Validator::Password->new(
	min_types => 1,
       )
    }

  if  ($Validator->Is_Valid($password,$password)) {
    return 1;
  } else {
    return 0;
  };


};

# ---------------------- MOP UP ---------------------------------

# speed up object construction
no Moose;
__PACKAGE__->meta->make_immutable;

1;

# remaining POD sections

# add your name here if you have worked on this code!

=head1 AUTHOR

Frossie Economou E<lt>frossie@noao.edu<gt>

=cut
